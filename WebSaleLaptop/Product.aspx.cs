﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Product : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public IQueryable<SanPham> SanPhams;
        public void Page_Load()
        {
            SanPhams = db.SanPhams.Select(sp => sp);
            GridView1.DataSource = SanPhams;

            GridView1.DataBind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = (e.RowIndex);
            int id = (int.Parse(GridView1.Rows[index].Cells[0].Text));
           SanPham sp = db.SanPhams.Single(k => k.MaSP == id);
            db.SanPhams.DeleteOnSubmit(sp);
            db.SubmitChanges();
            Page_Load();
        }

        protected void BntTim_Click(object sender, EventArgs e)
        {
            GridView1.DataSource = db.SanPhams.Where(q => q.TenSP.Contains(TxtTimkiem.Text)).ToList();
            GridView1.DataBind();
        }
        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton editButton = sender as LinkButton;
            int id = Int32.Parse(editButton.CommandArgument);
            Response.Redirect("EditSP?id=" + id.ToString());
        }
    }
}