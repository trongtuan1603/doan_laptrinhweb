﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace WebSaleLaptop
{
    public partial class DetailProduct : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        string id;
        public SanPham product;
        public List<SanPham> sameProducts;
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            product = db.SanPhams.Where(sp => sp.MaSP == Int32.Parse(id)).First();
            loadSameProducts();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var product = db.SanPhams.Where(sp => sp.MaSP == Int32.Parse(id)).First();
            ProductCart productCart = new ProductCart();
            productCart.maSP = product.MaSP;
            productCart.anhSP = product.AnhSP;
            productCart.tenSP = product.TenSP;
            productCart.soLuong = 1;
            productCart.giaBan = (long)product.GiaBan;


            if (Request.Cookies["cart"] == null)
            {
                List<ProductCart> lst = new List<ProductCart>();
                lst.Add(productCart);
                HttpCookie cart = new HttpCookie("cart");
                string json = new JavaScriptSerializer().Serialize(lst);
                cart.Value = json;
                cart.Expires = DateTime.Now.AddHours(1);
                Response.SetCookie(cart);
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                bool exist = false;
                JavaScriptSerializer js = new JavaScriptSerializer();
                List<ProductCart> productsInCart = js.Deserialize<List<ProductCart>>(Request.Cookies["cart"].Value);
                Response.Cookies["cart"].Expires = DateTime.Now.AddDays(-1);
                foreach(ProductCart item in productsInCart)
                {
                    if(item.maSP == Int32.Parse(id))
                    {
                        exist = true;
                        item.soLuong++;
                    }
                }
                if(!exist)
                    productsInCart.Add(productCart);
                HttpCookie cart = new HttpCookie("cart");
                string json = new JavaScriptSerializer().Serialize(productsInCart);
                cart.Value = json;
                cart.Expires = DateTime.Now.AddHours(1);
                Response.SetCookie(cart);
                Response.Redirect(Request.RawUrl);
            }
        }
        protected void loadSameProducts ()
        {
            sameProducts = db.SanPhams.Where(pr => pr.MaSP != Int32.Parse(id) && pr.MaLoaiSP == product.MaLoaiSP).Take(4).ToList();
        }

        protected void btnBuynow_Click(object sender, EventArgs e)
        {

        }
    }
}