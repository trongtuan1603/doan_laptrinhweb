﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class News : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public IQueryable<TinTuc> tintuc;
        public void Page_Load()
        {
            tintuc = db.TinTucs.Select(tt => tt);
            GridView1.DataSource = tintuc;

            GridView1.DataBind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = (e.RowIndex);
            int id = (Int32.Parse(GridView1.Rows[index].Cells[0].Text));
            TinTuc t = db.TinTucs.Single(k => k.ID == id);
            db.TinTucs.DeleteOnSubmit(t);
            db.SubmitChanges();
            Page_Load();
        }
        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton editButton = sender as LinkButton;
            int id = Int32.Parse(editButton.CommandArgument);
            Response.Redirect("EditNews?id=" + id.ToString());
        }
    }
    }