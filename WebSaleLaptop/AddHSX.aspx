﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddHSX.aspx.cs" Inherits="WebSaleLaptop.AddHSX" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                  <link href="Assets/css/form.css" rel="stylesheet" />
        <link href="Assets/css/admin.css" rel="stylesheet" />

    <div class="p-title-page"> 
       <ion-icon name="people-outline"></ion-icon>
       <p class="title-page">Thêm thương hiệu</p>
    </div>
    <div class="form-column" style="width: 600px">
        <div class="line">
            <asp:Label ID="LbTenHang" runat="server" Text="Tên hãng"></asp:Label>
                <asp:RequiredFieldValidator ID="ReqTenhang" ErrorMessage="Vui lòng nhập tên hãng" 
            ControlToValidate="TxtTenhang" runat="server" ForeColor="Red" />
        </div>
        <asp:TextBox ID="TxtTenHang" runat="server"></asp:TextBox>

        <div class="line">
            <asp:Label Text="Logo" runat="server" />
            <asp:RequiredFieldValidator ID="RequpladImg" ErrorMessage="Vui lòng chọn ảnh" ControlToValidate="uploadImg" runat="server" ForeColor="Red" />
        </div>
        <asp:FileUpload ID="uploadImg" runat="server" />
        <asp:Button ID="BtnThem" runat="server" Text="Thêm" OnClick="BtnThem_Click"  /> 
        <asp:Label ID="Lberror" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
