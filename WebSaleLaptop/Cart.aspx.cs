﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Cart : System.Web.UI.Page
    {
        public int count;
        public List<ProductCart> productsInCart;
        public int total_amount;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                loadDataCart();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Cookies["cart"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect(Request.RawUrl);
        }
        void loadDataCart ()
        {
            if(Request.Cookies["cart"] != null)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                productsInCart = js.Deserialize<List<ProductCart>>(Request.Cookies["cart"].Value);
                calTotalCart();
                count = productsInCart.Count();
            }
            GridView1.DataSource = productsInCart;
            GridView1.DataBind();
            
        }

        protected void btnContinueShopping_Click(object sender, EventArgs e)
        {

        }

        protected void calTotalCart()
        {
            foreach(ProductCart item in productsInCart)
            {
                total_amount += Int32.Parse(item.giaBan.ToString()) * item.soLuong;
            }

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<ProductCart> productsInCart = js.Deserialize<List<ProductCart>>(Request.Cookies["cart"].Value);
            Response.Cookies["cart"].Expires = DateTime.Now.AddDays(-1);
            foreach (ProductCart item in productsInCart)
            {
                if (item.maSP == Int32.Parse(e.CommandArgument.ToString()))
                {
                    if (e.CommandName == "decrease")
                    {
                        if(item.soLuong > 1)
                        {
                            item.soLuong--;
                        }
                    }  

                    if (e.CommandName == "increase")
                        item.soLuong++;
                }
            }
            HttpCookie cart = new HttpCookie("cart");
            string json = new JavaScriptSerializer().Serialize(productsInCart);
            cart.Value = json;
            cart.Expires = DateTime.Now.AddHours(1);
            Response.SetCookie(cart);
            Response.Redirect(Request.RawUrl);
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            if(Session["login"] == null)
            {
                Response.Redirect("Login.aspx?action=order");
            }
            else
            {
                Response.Redirect("Order.aspx");
            }
        }
    }
}