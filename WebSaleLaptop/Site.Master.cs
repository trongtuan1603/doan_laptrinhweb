﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class SiteMaster : MasterPage
    {
        public int countProduct;
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public KhachHang kh;
        public string check;
        protected void Page_Load(object sender, EventArgs e)
        {
            loadCountProduct();
            loadInfoCustomer();
        }

        public void loadCountProduct()
        {
            if (Request.Cookies["cart"] != null){
                JavaScriptSerializer js = new JavaScriptSerializer();
                List<ProductCart> productsInCart = js.Deserialize<List<ProductCart>>(Request.Cookies["cart"].Value);
                countProduct = productsInCart.Count();
            }
        }
        public void loadInfoCustomer()
        {
            check = Session["Login"] != null ? Session["Login"].ToString() : null;
            if (check != null)
            {
                kh = db.KhachHangs.Where(kh => kh.MaKH == Int32.Parse(check)).FirstOrDefault();
            }
        }
        public void btnSearchClick(string text)
        {
            Response.Redirect("Default.aspx?searchstr=" + text);
        }
        protected void Logout(object sender, EventArgs e)
        {
            Session["Login"] = null;
            Response.Redirect(Request.RawUrl);
        }

    }
}