﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditNews.aspx.cs" Inherits="WebSaleLaptop.EditNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Assets/css/form.css" rel="stylesheet" />
    <style>
    </style>
    <h1> Sửa Tin Tức</h1><br />
     <asp:Label ID="LbTitle" runat="server" Text="Title"></asp:Label>
    <asp:TextBox ID="TxtTitle" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="ReqTitle" ErrorMessage="Vui lòng nhập Title" 
        ControlToValidate="TxtTitle" runat="server" ForeColor="Red" />
    <br />
     <asp:Label ID="LbContent" runat="server" Text="Content"></asp:Label>
    <asp:TextBox ID="TxtContent" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="ReqContent" ErrorMessage="Vui lòng nhập Content" 
        ControlToValidate="TxtContent" runat="server" ForeColor="Red" />
    <br />
    <asp:Label Text="Image" runat="server" />
     <asp:FileUpload ID="uploadImg" runat="server" onchange="ChonAnh(this.value)"/>
    <asp:TextBox ID="TxtImage" runat="server"></asp:TextBox>    
    <asp:RequiredFieldValidator ID="RequpladImg" ErrorMessage="Vui lòng chọn Image" 
        ControlToValidate="TxtImage" runat="server" ForeColor="Red" />
    <br />
     <asp:Button ID="BtnUpdate" runat="server" Text="Update"  /> 
    <asp:Label ID="Lberror" runat="server" Text="" ForeColor="Red"></asp:Label>
    <script>
            function ChonAnh(a) {
                document.getElementById('<%=TxtImage.ClientID %>').value = a.split("\\").pop();
        }
    </script>
</asp:Content>
