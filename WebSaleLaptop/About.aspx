﻿<%@ Page Title="Giới thiệu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebSaleLaptop.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <div class="container">
        <div class="row" style="margin-top: 115px">
            <div class="col-md-6">
                <p style="font-size: 18pt; font-weight: 500">Chào mừng bạn đến với Laptop140</p>
                <p style="font-size: 14pt">Cửa hàng Laptop140 chuyên cung cấp các sản phẩm laptop xách tay Mỹ, máy tính để bàn chính hãng zin 100%, các loại linh kiện vi tính chính hãng... Đến với Worklap, người dùng sẽ được trải nghiệm các thiết bị máy tính laptop xách tay chất lượng với nhiều phân khúc giá, thương hiệu, phù hợp với nhiều nhu cầu sử dụng khác nhau.
                    <br />
                    <br />
                    Qua nhiều năm hoạt động trong lĩnh vực phân phối, mua – bán các thiết bị công nghệ, sửa chữa bảo trì máy tính cho các đơn vị, công ty, khách hàng cá nhân. Với đối ngũ nhân viên, kỹ thuật viên kinh nghiệm chuyên môn cao, có am hiểu về máy tính… sẽ phục khách hàng một cách nhiệt tình.
                    <br />
                    <br />
                    Chúng tôi luôn đặt chất lượng sản phẩm lên hàng đầu, đảm bảo cho khách hàng luôn cảm thấy rất yên tâm khi mua sắm và sử dụng các sản phẩm của cửa hàng Worklap. Sản phẩm của vi tính Worklap đa dạng từ Máy tính bàn, laptop xách tay, linh kiện máy tính… Các thiết bị đều đế từ những thương hiệu nổi tiếng toàn thế giới như: Dell,  HP, Lenovo,… </p>
            </div>
            <div class="col-md-6">
                <img src="Assets/images/c595c9dc092cb7fbfecedde02a6952ae.png" alt="" style="max-height: 100vh; width: 30vw"/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <img src="Assets/images/images_brands_asus_2020_2020_asus_rog_banner.jpg" alt="Alternate Text"  class="img-about"/>
            </div>
            <div class="col-lg-3">
                <img src="Assets/images/Banner-gaming_laptops.jpg" alt="Alternate Text"  class="img-about"/>
            </div>
            <div class="col-lg-3">
                <img src="Assets/images/KV_Banner_large.jpg" alt="Alternate Text"  class="img-about"/>
            </div>
            <div class="col-lg-3">
                <img src="Assets/images/Banner-main-01.png" alt="Alternate Text"  class="img-about"/>
            </div>
        </div>
    </div>
    <style>
        .img-about{
            height: 120px;
            width: 100%;
        }
    </style>
</asp:Content>
