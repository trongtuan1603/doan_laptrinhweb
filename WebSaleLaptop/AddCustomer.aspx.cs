﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace WebSaleLaptop
{
     
    public partial class AddCustomer : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
        }

        protected void BtnThem_Click(object sender, EventArgs e)
        {
            try
            {
                KhachHang kh = new KhachHang();
                kh.TenKH = TxtTenKH.Text;
                kh.DiaChi = TxtDiaChi.Text;
                kh.SDT = TxtSDT.Text;
                kh.NgaySinh = DateTime.Parse(TxtNgaySinh.Value);
                kh.GioiTinh = TxtGT.Text;
                kh.TaiKhoan = TxtTK.Text;
                kh.MatKhau = TxtMK.Text;
                kh.Email = TxtEmail.Text;
                db.KhachHangs.InsertOnSubmit(kh);
                db.SubmitChanges();  

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}