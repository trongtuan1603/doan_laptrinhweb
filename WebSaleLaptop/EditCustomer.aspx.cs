﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class EditCustomer : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        KhachHang khachHang = new KhachHang();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                try
                {
                    KhachHang khachHang1 = db.KhachHangs.Where(kh => kh.MaKH == Int32.Parse(Request.QueryString["id"])).FirstOrDefault();

                    khachHang1.TenKH = txtName.Text;
                    khachHang1.SDT = txtPhone.Text;
                    khachHang1.DiaChi = txtAddress.Text;
                    //khachHang.NgaySinh = txtBirth.SelectedDate.Date;
                    khachHang1.GioiTinh = txtGender.Text;
                    khachHang1.Email = txtEmail.Text;
                    db.SubmitChanges();
                    Response.Redirect("KhachHang.aspx");
                }
                catch (Exception)
                { 
                    throw;
                }
            }
            else
            {
                int id = Int32.Parse(Request.QueryString["id"]);
                if (id != -1)
                {
                    khachHang = db.KhachHangs.Where(kh => kh.MaKH == id).FirstOrDefault();
                }
                fillData();
            }
        }
        void fillData ()
        {
            txtName.Text = khachHang.TenKH;
            txtBirth.Value = khachHang.NgaySinh.Value.Date.ToString("yyyy-MM-dd");
            txtPhone.Text = khachHang.SDT;
            txtAddress.Text = khachHang.DiaChi;
            txtGender.Text = khachHang.GioiTinh;
            txtEmail.Text = khachHang.Email;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}