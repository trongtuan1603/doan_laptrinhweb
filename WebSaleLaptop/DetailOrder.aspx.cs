﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class DetailOrder : System.Web.UI.Page
    {
        public string order_id;
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        List<ChiTietDonHang> detail = new List<ChiTietDonHang>();
        protected void Page_Load(object sender, EventArgs e)
        {
            order_id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : null;
            if(order_id != null)
            {
                detail = db.ChiTietDonHangs.Where(ct => ct.MaDonHang == Int32.Parse(order_id)).ToList();
                GridView1.DataSource = detail;
                GridView1.DataBind();
            }
        }
    }
}