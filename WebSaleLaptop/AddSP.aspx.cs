﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class AddSP : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            List<HangSanXuat> hangSanXuat = db.HangSanXuats.Select(t => t).ToList();
            TxtHSX.DataSource = hangSanXuat;
            TxtHSX.DataBind();

            List<LoaiSP> loaiSPs = db.LoaiSPs.Select(t => t).ToList();
            TxtML.DataSource = loaiSPs;
            TxtML.DataBind();
        }

        protected void BtnThem_Click(object sender, EventArgs e)
        {
            try
            {
                SanPham sp = new SanPham();
                sp.TenSP = TxtTenSp.Text;
                sp.GiaBan = int.Parse(TxtGiaBan.Text);
                sp.RAM = int.Parse(TxtRam.Text);
                sp.CPU = TxtCPU.Text;
                sp.BoNho = TxtBN.Text;
                sp.MoTa = TxtMT.Text;
                sp.NgayNhap = DateTime.Parse(TxtNN.Value);
                sp.AnhSP = uploadImg.FileName;
                sp.SoLuong = int.Parse(TxtSL.Text);
                sp.MaLoaiSP = int.Parse(Request.Form[TxtML.UniqueID]);
                sp.HangSX = int.Parse(Request.Form[TxtHSX.UniqueID]);
                sp.SPMoi = int.Parse(TxtSPMoi.Text);
                var path = System.IO.Path.Combine(Server.MapPath("Assets/images/products/"), sp.AnhSP);
                if (System.IO.File.Exists(path))
                {
                    Lberror.Text = "Hình ảnh đã tồn tại";
                }
                else
                {
                    uploadImg.SaveAs(path);
                    db.SanPhams.InsertOnSubmit(sp);
                    db.SubmitChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}