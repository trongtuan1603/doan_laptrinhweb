﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddLoaiSP.aspx.cs" Inherits="WebSaleLaptop.AddLoaiSP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Assets/css/form.css" rel="stylesheet" />
    <style>
    </style>
    <h1> Thêm Loại Sản Phẩm</h1>
     <asp:Label ID="LbTenLoai" runat="server" Text="Tên loại"></asp:Label>
    <asp:TextBox ID="TxtTenLoai" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="ReqTenloai" ErrorMessage="Vui lòng nhập tên loai" 
        ControlToValidate="TxtTenLoai" runat="server" ForeColor="Red" />
    <br />
    <asp:Button ID="BtnThem" runat="server" Text="Thêm" OnClick="BtnThem_Click" /> 
</asp:Content>
