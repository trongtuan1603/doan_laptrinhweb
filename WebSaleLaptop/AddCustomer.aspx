﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddCustomer.aspx.cs" Inherits="WebSaleLaptop.AddCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Assets/css/form.css" rel="stylesheet" />
    <link href="Assets/css/admin.css" rel="stylesheet" />
    <style>
    </style>
        <div class="p-title-page"> 
            <ion-icon name="people-outline"></ion-icon>
            <p class="title-page"> Thêm khách hàng</p>
        </div>
    <div class="form-column" style="width: 500px;"> 
        <div class="line">
            <asp:Label ID="Label2" runat="server" Text="Tên khách hàng" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="ReqTenKH" ErrorMessage="Vui lòng nhập tên khách hàng" 
            ControlToValidate="TxtTenKH" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="TxtTenKH" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label3" runat="server" Text="Địa chỉ"></asp:Label>
            <asp:RequiredFieldValidator ID="ReqDiaChi" ErrorMessage="Vui lòng nhập địa chỉ" 
             ControlToValidate="TxtDiaChi" runat="server" ForeColor="Red" />
        </div>
        <asp:TextBox ID="TxtDiaChi" runat="server"></asp:TextBox>
         <div class="line">
                      <asp:Label ID="Label4" runat="server" Text="SĐT"></asp:Label>
                      <asp:RequiredFieldValidator ID="ReqSDT" ErrorMessage="Vui lòng nhập số điện thoại" 
            ControlToValidate="TxtSDT" runat="server" ForeColor="Red" />
         </div>
        <asp:TextBox ID="TxtSDT" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label5" runat="server" Text="Ngày Sinh"></asp:Label>
        </div>
        <input type="date" name="name" id="TxtNgaySinh" value="" runat="server"/>
        <div class="line">
         <asp:Label ID="Label6" runat="server" Text="Giới Tính"></asp:Label>
         <asp:RequiredFieldValidator ID="ReqGT" ErrorMessage="Vui lòng nhập giới tính" 
            ControlToValidate="TxtGT" runat="server" ForeColor="Red" />
            </div>
        <asp:TextBox ID="TxtGT" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label7" runat="server" Text="Tài Khoản"></asp:Label>
            <asp:RequiredFieldValidator ID="Reqtk" ErrorMessage="Vui lòng nhập tài khoảng" 
            ControlToValidate="TxtTK" runat="server" ForeColor="Red" />
        </div>
        <asp:TextBox ID="TxtTK" runat="server"></asp:TextBox>
        <div class="line">         
            <asp:Label ID="Label8" runat="server" Text="Mật Khẩu"></asp:Label>
            <asp:RequiredFieldValidator ID="ReqMK" ErrorMessage="Vui lòng nhập mật khẩu" 
            ControlToValidate="TxtDiaChi" runat="server" ForeColor="Red" />
        </div>
        <asp:TextBox ID="TxtMK" runat="server"></asp:TextBox>
        <div class="line">         
            <asp:Label ID="Label9" runat="server" Text="Email"></asp:Label>
            <asp:RequiredFieldValidator ID="ReqEmail" ErrorMessage="Vui lòng nhập Email" 
                ControlToValidate="TxtEmail" runat="server" ForeColor="Red" />
        </div>
        <asp:TextBox ID="TxtEmail" runat="server"></asp:TextBox>
        <asp:Button ID="BtnThem" runat="server" Text="Thêm" OnClick="BtnThem_Click" />
    </div>
    <style>
         input[type=date] {
            width: 100%;
            padding: 6px 10px;
            margin: 10px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
         }
         .label-form {
             flex: 1
         }
    </style>
</asp:Content>
