﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class LoginAdmin : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var admin = db.Admins.Where(acc => acc.UserAdmin == username.Value && acc.PassAdmin == password.Value).FirstOrDefault();
                if(admin != null)
                {
                    Session["admin"] = username.Value;
                    Response.Redirect("Admin.aspx");
                }
                else
                {
                    lblError.Text = "Tên đăng nhập hoặc mật khẩu không đúng !";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}