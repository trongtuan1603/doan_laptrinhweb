﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class AddLoaiSP : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnThem_Click(object sender, EventArgs e)
        {
            try
            {
                LoaiSP loai = new LoaiSP();
                loai.TenLoai = TxtTenLoai.Text;
               
                db.LoaiSPs.InsertOnSubmit(loai);
                db.SubmitChanges();
                Response.Redirect("LoaiSP.aspx");
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}