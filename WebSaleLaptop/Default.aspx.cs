﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class _Default : Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public static List<SanPham> products;
        public static List<SanPham> productsTemp;
        public List<TinTuc> news;
        public List<HangSanXuat> brands;
        public List<LoaiSP> categories;
        static long million = 1000000;
        public string searchStr;
        public string category_id;
        public string brandID;
        public static string brandName;
        public static int offset;
        protected void Page_Load(object sender, EventArgs e)
        {
            searchStr = Request.QueryString["searchstr"];
            brandID = Request.QueryString["brandid"];
            if (!this.IsPostBack)
            {
                if (searchStr != null)
                {
                    products = db.SanPhams.Where(sp => sp.TenSP.Contains(searchStr)).ToList();
                }
                else if(brandID != null)
                {
                    brandName = db.HangSanXuats.Where(br => br.HangSX == Int32.Parse(brandID)).FirstOrDefault().TenHang;
                    products = db.SanPhams.Where(sp => sp.HangSX == Int32.Parse(brandID)).ToList();
                    
                } 
                else
                {
                    products = db.SanPhams.Select(sp => sp).ToList();
                }
                productsTemp = products;
                updateOffset();
            }
            news = db.TinTucs.Select(tt => tt).Take(4).ToList();
            brands = db.HangSanXuats.Select(br => br).ToList();
            categories = db.LoaiSPs.Select(ct => ct).ToList();
            updateOffset();

        }
        protected void CheckBoxPriceChange(object sender, EventArgs e)
        {
            string value = FilterPrice.SelectedValue.ToString();
            string valueNeed = FilterNeed.SelectedValue.ToString();

            if (value != "" && valueNeed != "")
            {
                Range range = new Range();
                range.min = Int32.Parse(value.Split(',')[0]);
                range.max = Int32.Parse(value.Split(',')[1]);
                products = productsTemp.Where(sp => sp.GiaBan > range.min * million).Where(sp => sp.GiaBan < range.max * million).Where(sp=>sp.MaLoaiSP == Int32.Parse(valueNeed)).ToList();
                updateOffset();
            }
            else if(value != "" && valueNeed == "")
            {
                Range range = new Range();
                range.min = Int32.Parse(value.Split(',')[0]);
                range.max = Int32.Parse(value.Split(',')[1]);
                products = productsTemp.Where(sp => sp.GiaBan > range.min * million).Where(sp => sp.GiaBan < range.max * million).ToList();
                updateOffset();
            }
            else if (value == "" && valueNeed != "")
            {
                products = productsTemp.Where(sp => sp.MaLoaiSP == Int32.Parse(valueNeed)).ToList();
                updateOffset();
            }
            updateOffset();
            FilterNeed.Focus();
            //Page.SetFocus(Button1);
        }
        void updateOffset()
        {
            if (products.Count() < 12)
            {
                offset = products.Count();
                Button1.Visible = false;
            }
            else
            {
                Button1.Visible = true;
                offset = 12;
            }
        }
        public void Ascending_Price(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderBy(sp => sp.GiaBan).ToList();
            FilterPrice.Focus();

        }
        public void Descending_Price(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderByDescending(sp => sp.GiaBan).ToList();
        }
        public void SortByName(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderBy(sp => sp.TenSP).ToList();
        }
        public void SortByNameDescending(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderByDescending(sp => sp.TenSP).ToList();
        }

        public void SortByDateDescending(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderByDescending(sp => sp.NgayNhap).ToList();
        }
        public void SortBySaleDescending(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderByDescending(sp => sp).ToList();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            products = products.Select(sp => sp).OrderBy(sp => sp.GiaBan).ToList();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (offset + 12 <= products.Count())
            {
                offset += 12;
                Button2.Focus();
            }
            else
            {
                offset = products.Count();
                Button1.Visible = false;
                Button2.Focus();

            }
        }
    }

    public class Range
    {
        public int min { get; set; }
        public int max { get; set; }
    }
}