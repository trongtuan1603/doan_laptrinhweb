﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddNews.aspx.cs" Inherits="WebSaleLaptop.AddNews" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Assets/css/admin_news/addnews.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Assets/css/form.css" rel="stylesheet" />
    <div runat="server" class="container"> 
        <div class="form-add">
            <h2>THÊM SẢN PHẨM MỚI</h2>
             <asp:Label ID="LbTitle" runat="server" Text="Title"></asp:Label>
            <asp:TextBox ID="TxTTitle" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="ReqTitle" ErrorMessage="Vui lòng nhập Title" 
        ControlToValidate="TxtTitle" runat="server" ForeColor="Red" />
            <br />
             <asp:Label ID="LbContent" runat="server" Text="Content"></asp:Label>
            <asp:TextBox ID="TxTContent" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="ReqContent" ErrorMessage="Vui lòng nhập Content" 
        ControlToValidate="TxtContent" runat="server" ForeColor="Red" />
            <br />
            <asp:Label Text="Image" runat="server" />
            <asp:FileUpload ID="uploadImg" runat="server" />
    <asp:RequiredFieldValidator ID="RequpladImg" ErrorMessage="Vui lòng chọn ảnh" ControlToValidate="uploadImg" runat="server" ForeColor="Red" />
    <br />
            <asp:Button ID="btnAdd" runat="server" Text="Thêm" OnClick="btnAdd_Click" />
            <asp:Label ID="lblError" runat="server"></asp:Label>
        </div>
    </div>
 
</asp:Content>
