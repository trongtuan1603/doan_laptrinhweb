﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="LoaiSP.aspx.cs" Inherits="WebSaleLaptop.LoaiSP1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page"> Quản lý loại sản phẩm</p>
    </div>
    <div class="add-and-find" style="margin-bottom: 20px;">
        <a href="AddLoaiSP.aspx"> Thêm loại sản phẩm</a>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="MaLoai" DataSourceID="SqlDataSource1" CssClass="table-manager" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="MaLoai" HeaderText="Mã Loại" InsertVisible="False" ReadOnly="True" SortExpression="MaLoai" />
            <asp:BoundField DataField="TenLoai" HeaderText="Tên loại" SortExpression="TenLoai" />
            <asp:CommandField ShowDeleteButton="True" />
            <asp:CommandField ShowEditButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QLWebBanLaptopConnectionString %>" SelectCommand="SELECT * FROM [LoaiSP]"  
        UpdateCommand="UPDATE [LoaiSP] SET [TENLOAI] = @TENLOAI WHERE [MALOAI] = @MALOAI"
        DeleteCommand="DELETE FROM [LOAISP] WHERE [MALOAI] = @MALOAI">

    </asp:SqlDataSource>
        <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
    </style>
</asp:Content>
