﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="OrderManager.aspx.cs" Inherits="WebSaleLaptop.OrderManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page"> Quản lý đơn hàng</p>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="MaDonHang" DataSourceID="SqlDataSource1" CssClass="table-manager" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="MaDonHang" HeaderText="Mã đơn hàng" InsertVisible="False" ReadOnly="True" SortExpression="MaDonHang" />
            <asp:BoundField DataField="NgayDat" HeaderText="Ngày đặt" SortExpression="NgayDat" />
            <asp:BoundField DataField="MaKH" HeaderText="Mã khách hàng" SortExpression="MaKH" />
            <asp:TemplateField><ItemTemplate>
                                 <asp:LinkButton ID="EditButton" runat="server" CommandArgument='<%# Eval("MaDonHang") %>' Text="Chi tiết đơn hàng" onclick="Details_Click"></asp:LinkButton>
                        
            </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QLWebBanLaptopConnectionString %>" SelectCommand="SELECT * FROM [DonHang]"></asp:SqlDataSource>
    <style>
        .table-manager {
            width: 100%;
            margin-top: 20px;
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
    </style>
</asp:Content>
