﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class AddHSX : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnThem_Click(object sender, EventArgs e)
        {
            try
            {
                HangSanXuat hsx = new HangSanXuat();
                hsx.TenHang = TxtTenHang.Text;
                hsx.Logo = uploadImg.FileName;
             
                var path = System.IO.Path.Combine(Server.MapPath("Assets/images/brands/"), hsx.Logo);
                if (System.IO.File.Exists(path))
                {
                    Lberror.Text = "Hình ảnh đã tồn tại";
                }
                else
                {
                    uploadImg.SaveAs(path);
                    db.HangSanXuats.InsertOnSubmit(hsx);
                    db.SubmitChanges();
                    Response.Redirect("HangSanXuat.aspx");

                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}