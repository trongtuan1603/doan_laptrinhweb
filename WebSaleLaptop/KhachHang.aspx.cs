﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WebSaleLaptop
{
    public partial class KhachHang1 : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public IQueryable<KhachHang> khachHangs;
        public void Page_Load()
        {
            if (!Page.IsPostBack)
            {
                //QueryActiveTasks();
            }
            khachHangs = db.KhachHangs.Select(sp=>sp);
            GridView1.DataSource = khachHangs;

           GridView1.DataBind();
           
        }
        public void AddkH()
        {
            KhachHang kh = new KhachHang();
            db.KhachHangs.InsertOnSubmit(kh);
            db.SubmitChanges();
        }

        protected void BntTim_Click(object sender, EventArgs e)
        {
            GridView1.DataSource = db.KhachHangs.Where(q => q.TenKH.Contains(TxtTimkiem.Text)).ToList();
            GridView1.DataBind();
            
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            Page_Load();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = (e.RowIndex);
            int id = (Int32.Parse(GridView1.Rows[index].Cells[0].Text));
            KhachHang kh = db.KhachHangs.Single(k => k.MaKH == id);
            db.KhachHangs.DeleteOnSubmit(kh);
            db.SubmitChanges();
            Page_Load();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            Page_Load();
        }
        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton editButton = sender as LinkButton;
            int id = Int32.Parse(editButton.CommandArgument);
            Response.Redirect("EditCustomer?id=" + id.ToString());
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
        }
        protected void linkbtnThem_Click(object sender, EventArgs e)
        {

        }
    }
}