﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class EditHSX : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        HangSanXuat hsx = new HangSanXuat();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Lberror.Text = "";

                try
                {

                    HangSanXuat hs = db.HangSanXuats.Where(s => s.HangSX == Int32.Parse(Request.QueryString["id"])).FirstOrDefault();
                    hs.TenHang = TxtTenHang.Text;
                    hs.Logo = TxtLogo.Text;
                   
                    if (uploadImg.FileName != null)
                    {
                        var path = System.IO.Path.Combine(Server.MapPath("Assets/images/news/"), hs.Logo); 
                        if (System.IO.File.Exists(path))
                        {
                            Lberror.Text = "Hình ảnh đã tồn tại";
                        }
                        else
                        {
                            uploadImg.SaveAs(path);

                        }
                    }

                    db.SubmitChanges();
                    Response.Redirect("HangSanXuat.aspx");
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                int id = Int32.Parse(Request.QueryString["id"]);
                if (id != -1)
                {
                    hsx = db.HangSanXuats.Where(h => h.HangSX == id).FirstOrDefault();
                }
                fillData();
            }


            void fillData()
            {
                TxtTenHang.Text = hsx.TenHang;
                TxtLogo.Text = hsx.Logo;
                
            }
        }
    }
}