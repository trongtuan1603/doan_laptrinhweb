﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class EditNews : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        TinTuc tintuc = new TinTuc();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Lberror.Text = "";

                try
                {

                    TinTuc tt = db.TinTucs.Where(t => t.ID == Int32.Parse(Request.QueryString["id"])).FirstOrDefault();
                    tt.Title = TxtTitle.Text;
                    tt.Content = TxtContent.Text;
                    tt.Image = TxtImage.Text;

                    if (uploadImg.FileName != null)
                    {
                        var path = System.IO.Path.Combine(Server.MapPath("Assets/images/news/"), tt.Image);
                        if (System.IO.File.Exists(path))
                        {
                            Lberror.Text = "Hình ảnh đã tồn tại";
                        }
                        else
                        {
                            uploadImg.SaveAs(path);

                        }
                    }

                    db.SubmitChanges();
                    Response.Redirect("News.aspx");
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                int id = Int32.Parse(Request.QueryString["id"]);
                if (id != -1)
                {
                    tintuc = db.TinTucs.Where(h => h.ID == id).FirstOrDefault();
                }
                fillData();
            }


            void fillData()
            {
                TxtTitle.Text =tintuc.Title;
                TxtContent.Text = tintuc.Content;
                TxtImage.Text = tintuc.Image;

            }
        }
    }
}