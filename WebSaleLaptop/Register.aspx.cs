﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Register : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public string error;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                HandleRegister();
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {

        }

        protected void HandleRegister()
        {
            KhachHang kh = new KhachHang();
            kh.TenKH = txtName.Text;
            kh.SDT = txtPhone.Text;
            kh.DiaChi = txtAddress.Text;
            kh.GioiTinh = DropDownList1.SelectedItem.Text;
            kh.MatKhau = Convert.ToBase64String(Encoding.UTF8.GetBytes(txtPassword.Text));
            kh.Email = txtEmail.Text;

            try
            {
                if (CheckExist(kh))
                {
                    db.KhachHangs.InsertOnSubmit(kh);
                    db.SubmitChanges();
                    Session["Login"] = kh.MaKH;
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    error = "Số điện thoại hoặc email đã được đăng ký. Vui lòng đăng nhập !";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected bool CheckExist(KhachHang kh)
        {
            KhachHang check = db.KhachHangs.Where(k => k.SDT == kh.SDT || k.Email == kh.Email).FirstOrDefault();
            if(check != null)
            {
                return false;
            }
            return true;
        }
    }
}