﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class HangSanXuat1 : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public IQueryable<HangSanXuat> hsx;
        public void Page_Load()
        {
            hsx = db.HangSanXuats.Select(sp => sp);
            GridView1.DataSource = hsx;

            GridView1.DataBind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = (e.RowIndex);
            int id = (Int32.Parse(GridView1.Rows[index].Cells[0].Text));
            HangSanXuat h = db.HangSanXuats.Single(k => k.HangSX == id);
            db.HangSanXuats.DeleteOnSubmit(h);
            db.SubmitChanges();
            Page_Load();
        }
        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton editButton = sender as LinkButton;
            int id = Int32.Parse(editButton.CommandArgument);
            Response.Redirect("EditHSX?id=" + id.ToString());
        }
    }
}