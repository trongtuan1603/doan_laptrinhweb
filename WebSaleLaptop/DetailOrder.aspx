﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="DetailOrder.aspx.cs" Inherits="WebSaleLaptop.DetailOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page">Chi tiết đơn hàng <%=order_id%></p>
    </div>
    <asp:GridView ID="GridView1" runat="server" CssClass="table-manager" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Madonhang" HeaderText="Mã đơn hàng" />
            <asp:BoundField DataField="masp" HeaderText="Mã sản phẩm" />
            <asp:BoundField DataField="soluong" HeaderText="Số lượng" />
            <asp:BoundField DataField="thanhtien" HeaderText="Thành tiền" />
        </Columns>
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#E3EAEB" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F8FAFA" />
        <SortedAscendingHeaderStyle BackColor="#246B61" />
        <SortedDescendingCellStyle BackColor="#D4DFE1" />
        <SortedDescendingHeaderStyle BackColor="#15524A" />

    </asp:GridView>
    <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
    </style>
</asp:Content>
