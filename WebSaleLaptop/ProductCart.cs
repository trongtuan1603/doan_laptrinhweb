﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSaleLaptop
{
    public class ProductCart
    {
            public int maSP { get; set; }
            public string anhSP { get; set; }
            public string tenSP { get; set; }
            public int soLuong { get; set; }
            public long giaBan { get; set; }
    }
}