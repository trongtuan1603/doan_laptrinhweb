﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetailProduct.aspx.cs" Inherits="WebSaleLaptop.DetailProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/detail_product.css" rel="stylesheet"/>
    <link href="Assets/css/product.css" rel="stylesheet" />
    <form runat="server" style="margin-top: 110px; min-height: 900px;">
        <div style="display: flex; flex-direction: column">
            <div class="container" style="margin-top: 20px">
                <div class="col-md-5" id="left-content">
                    <div class="border-image">
                        <img class="product-img" src="Assets/images/products/<%=product.AnhSP%>" alt="Alternate Text" />
                    </div>
                </div>
                <div class="col-md-7" id="right-content">
                    <div class="product-info">
                        <h3 class="product-name"><%=product.TenSP%></h3>
                        <p class="price-product"><%=String.Format("{0:0,0}", product.GiaBan) %>₫</p>
                        <div class="detail-info">
                            <p class="title-detail">Thông tin sản phẩm</p>
                            <div class="detail-line">
                                <p>Dung lượng RAM: <b><%=product.RAM%>GB</b></p>
                                <p>Bộ nhớ: <b><%=product.BoNho%>GB</b></p>
                            </div>
                            <div class="detail-line">
                                <p>Vi xử lý: <b><%=product.CPU%></b></p>
                                <p>Hãng sản xuất: <b><%=product.HangSanXuat.TenHang%></b></p>
                            </div>
                        </div>
                        <div class="action-buy">
                            <asp:Button ID="Button1" runat="server" Text="Thêm vào giỏ hàng" OnClick="Button1_Click" CssClass="btn-add-to-cart" />
                            <%--<asp:Button ID="btnBuynow" runat="server" Text="Mua ngay" CssClass="btn-buy-now" OnClick="btnBuynow_Click" />--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="desciption-info">
                <p class="title-description">Mô tả về sản phẩm</p>
                <p class="description-text"><%=product.MoTa%></p>
            </div>
            <div class="same-products">
                <p class="title-same">Top sản phẩm cùng loại</p>
                <%if (sameProducts.Count > 0){ %>
                <%foreach (var sp in sameProducts){ %>
                    <div class="item-product" onclick="viewDetail('DetailProduct.aspx?id=<%=sp.MaSP%>')">
                        <div>
                            <div class="img-product">
                                <img class="img" src="Assets/images/products/<%=sp.AnhSP%>" alt="img-product"/>
                            </div>
                            <p class="price-text"><%=String.Format("{0:0,0}", sp.GiaBan) %>₫</p>
                            <p class="name"#"><%=sp.TenSP%></p>
                            <div class="thongso">
                                <img src="Assets/images/cpu.jpg" class="icon-thongso" />
                                <p><%=sp.CPU%></p>
                            </div>
                            <div class="thongso">
                                <div style="flex: 1; flex-direction: row; display: flex; align-items: center">
                                    <img src="Assets/images/ram.png" class="icon-thongso" />
                                    <p><%=sp.RAM%></p>
                                </div>
                                <div class="thongso">
                                    <img src="Assets/images/storage.jpg" class="icon-thongso" />
                                    <p><%=sp.BoNho%></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <%}%>
           <%} %>
            </div>
        </div>
    </form>
    <script>
        function viewDetail(link) {
            window.location.href = link;
        }
    </script>
    <style>
        .same-products {
            padding: 10px;
        }
        .same-products .item-product{
            margin: 0 40px 10px 0;
            transition: 0s;
        }
        .title-same {
            font-size: 13pt;
            font-weight: bold;
        }
    </style>
</asp:Content>
