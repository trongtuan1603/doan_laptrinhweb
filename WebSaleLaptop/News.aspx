﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="WebSaleLaptop.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page"> Quản lý tin tức</p>
    </div>
    <asp:LinkButton ID="linkbtnThem" runat="server" ><a href="AddNews.aspx"> Thêm</a></asp:LinkButton>   <br />    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"  OnRowDeleting="GridView1_RowDeleting" CssClass="table-manager">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="Content" HeaderText="Content" />
            <asp:BoundField DataField="Image" HeaderText="Image" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <Columns>
               <asp:TemplateField>
                  <ItemTemplate>
                      <asp:LinkButton ID="EditButton" runat="server" CommandArgument='<%# Eval("ID") %>' Text="Edit" onclick="Details_Click"></asp:LinkButton>
                   </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
    </asp:GridView>
    <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
    </style>
</asp:Content>
