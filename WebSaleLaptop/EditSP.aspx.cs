﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class EditSP : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        SanPham sanpham= new SanPham();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                try
                {
                    SanPham sp = db.SanPhams.Where(s => s.MaSP == Int32.Parse(Request.QueryString["id"])).FirstOrDefault();
                    sp.TenSP = TxtTenSp.Text;
                    sp.GiaBan = int.Parse(TxtGiaBan.Text);
                    sp.RAM = int.Parse(TxtRam.Text);
                    sp.CPU = TxtCPU.Text;
                    sp.BoNho = TxtBN.Text;
                    sp.MoTa = TxtMT.Text;
                    sp.NgayNhap = DateTime.Parse(TxtNN.Text);
                    sp.AnhSP = uploadImg.FileName;
                    sp.SoLuong = int.Parse(TxtSL.Text);
                    sp.MaLoaiSP = int.Parse(TxtML.Text);
                    sp.HangSX = int.Parse(TxtHSX.Text);
                    sp.SPMoi = int.Parse(TxtSPMoi.Text);
                    var path = System.IO.Path.Combine(Server.MapPath("Assets/images/products/"), sp.AnhSP);
                    if (System.IO.File.Exists(path))
                    {
                        Lberror.Text = "Hình ảnh đã tồn tại";
                    }
                    else
                    {
                        uploadImg.SaveAs(path);
                        db.SanPhams.InsertOnSubmit(sp);
                        db.SubmitChanges();
                    }
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                int id = Int32.Parse(Request.QueryString["id"]);
                if (id != -1)
                {
                    sanpham = db.SanPhams.Where(sp => sp.MaSP == id).FirstOrDefault();
                }
                fillData();
            }
            void fillData()
            {
                TxtTenSp.Text = sanpham.TenSP;
                TxtGiaBan.Text = (sanpham.GiaBan).ToString();
                TxtRam.Text=(sanpham.RAM).ToString();
                TxtCPU.Text = sanpham.CPU;
                TxtBN.Text = sanpham.BoNho ;
                TxtMT.Text=sanpham.MoTa ;
                 TxtNN.Text=(sanpham.NgayNhap).ToString() ;
                sanpham.AnhSP = uploadImg.FileName;
                TxtSL.Text =(sanpham.SoLuong).ToString() ;
                TxtML.Text = (sanpham.MaLoaiSP).ToString();
                TxtHSX.Text = (sanpham.HangSX).ToString();
                TxtSPMoi.Text = (sanpham.SPMoi).ToString();
            }

        }
    }
}