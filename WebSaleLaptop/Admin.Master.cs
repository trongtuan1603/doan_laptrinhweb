﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        public string adminName;
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["admin"] == null)
            {
                Response.Redirect("LoginAdmin");
            }
            else
            {
                adminName = db.Admins.Where(ad => ad.UserAdmin == Session["admin"].ToString()).FirstOrDefault().HoTen;
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["admin"] = null;
            Response.Redirect("LoginAdmin");
        }
    }
}