﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Login : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public string error;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                handleLogin();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

        }

        protected void handleLogin()
        {
            try
            {
                var result = db.KhachHangs.Where(kh => kh.SDT == txtPhone.Text || kh.Email == txtPhone.Text).Where(pass => pass.MatKhau == Convert.ToBase64String(Encoding.UTF8.GetBytes(txtPassword.Text)));
                if(result.Count() > 0)
                {
                    if(Request.QueryString["action"] == "order")
                    {
                        Session["Login"] = result.FirstOrDefault().MaKH;
                        Response.Redirect("Order.aspx");
                    }
                    else
                    {
                        Session["Login"] = result.FirstOrDefault().MaKH;
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    error = "Tài khoản hoặc mật khẩu không chính xác. Vui lòng kiểm tra lại !";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void txtPhone_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}