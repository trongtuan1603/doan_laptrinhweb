﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class Order : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();
        public static List<ProductCart> productsInCart;
        public int total_amount;
        public static KhachHang kh;
        public bool success;
        protected void Page_Load(object sender, EventArgs e)
        {
            string check = Session["Login"] != null ? Session["Login"].ToString() : null;
            if (!Page.IsPostBack)
            {
                if (check != null)
                {
                    kh = db.KhachHangs.Where(kh => kh.MaKH == Int32.Parse(check)).FirstOrDefault();
                    loadDataOrder();
                }
                else
                    Response.Redirect("Login.aspx?action=order");
            }
            else
            {

            }
        }
        void loadDataOrder()
        {
            if (Request.Cookies["cart"] != null)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                productsInCart = js.Deserialize<List<ProductCart>>(Request.Cookies["cart"].Value);
                calTotalCart();
            }

        }

        protected void calTotalCart()
        {
            foreach (ProductCart item in productsInCart)
            {
                total_amount += Int32.Parse(item.giaBan.ToString()) * item.soLuong;
            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                DonHang order = new DonHang();
                order.NgayDat = DateTime.Now;
                order.MaKH = kh.MaKH;

                db.DonHangs.InsertOnSubmit(order);
                db.SubmitChanges();

                foreach (var product in productsInCart)
                {
                    ChiTietDonHang detail = new ChiTietDonHang();
                    detail.MaDonHang = order.MaDonHang;
                    detail.MaSP = product.maSP;
                    detail.SoLuong = product.soLuong;
                    detail.ThanhTien = product.giaBan * product.soLuong;
                    db.ChiTietDonHangs.InsertOnSubmit(detail);
                }
                db.SubmitChanges();
                Response.Cookies["cart"].Expires = DateTime.Now.AddDays(-1);
                success = true;
                Page.RegisterStartupScript("refresh", "<script language=javascript>window.setTimeout('goHome()',3000);</script>");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}