﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class OrderManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton editButton = sender as LinkButton;
            int id = Int32.Parse(editButton.CommandArgument);
            Response.Redirect("DetailOrder?id=" + id.ToString());
        }
    }
}