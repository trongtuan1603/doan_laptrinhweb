﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSaleLaptop
{
    public partial class AddNews : System.Web.UI.Page
    {
        DBSaleLaptopDataContext db = new DBSaleLaptopDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                TinTuc tinTuc = new TinTuc();
                tinTuc.Title = TxTTitle.Text;
                tinTuc.Content = TxTContent.Text;
                tinTuc.Image = uploadImg.FileName;

                var path = System.IO.Path.Combine(Server.MapPath("Assets/images/news/"), tinTuc.Image);
                if (System.IO.File.Exists(path))
                {
                    lblError.Text = "Hình ảnh đã tồn tại";
                }
                else
                {
                    uploadImg.SaveAs(path);
                    db.TinTucs.InsertOnSubmit(tinTuc);
                    db.SubmitChanges();
                    Response.Redirect("News.aspx");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}