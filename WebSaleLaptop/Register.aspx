﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebSaleLaptop.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/resgister.css" rel="stylesheet" />
    <form runat="server" class="register-form">
        <div class="form-column" runat="server" style="width: 400px; box-shadow: 0 0 5px gray; border-radius: 5px; padding: 20px">
            <%if (error == null) { %>
                <h4 style="text-align: center; margin-top: 20px">Đăng ký tài khoản</h4>
                <asp:Label ID="lbName" runat="server" Text="Họ tên"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Vui lòng nhập tên" ControlToValidate="txtName" ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtName" runat="server" CssClass="info-feild"></asp:TextBox>
                <asp:Label ID="lblPhone" runat="server" Text="Số điện thoại"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Vui lòng nhập số điện thoại" ControlToValidate="txtPhone" ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPhone" runat="server" CssClass="info-feild"></asp:TextBox>
                <asp:Label ID="lblAddress" runat="server" Text="Địa chỉ"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Vui lòng nhập địa chỉ" ControlToValidate="txtAddress"  ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtAddress" runat="server" CssClass="info-feild"></asp:TextBox>
                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Vui lòng nhập địa chỉ email" ControlToValidate="txtEmail"  ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="info-feild"></asp:TextBox>
                <asp:Label ID="lblGender" runat="server" Text="Giới tính"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="info-feild">
                    <asp:ListItem Value="male">Nam</asp:ListItem>
                    <asp:ListItem Value="female">Nữ</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblPassword" runat="server" Text="Mật khẩu"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Mật khẩu không được để trống" ControlToValidate="txtPassword"  ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="info-feild"></asp:TextBox>
                <asp:Label ID="lblRePassword" runat="server" Text="Nhập lại mật khẩu"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Mật khẩu không được để trống" ControlToValidate="txtRePassword"  ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtRePassword" runat="server" TextMode="Password" CssClass="info-feild"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtRePassword" ErrorMessage="Mật khẩu không khớp" ForeColor="Red" CssClass="validator-register"></asp:CompareValidator>
                <asp:Button ID="btnRegister" runat="server" Text="Đăng ký" CssClass="btn-register" OnClick="btnRegister_Click"/>
            <%} else { %>
                <p style="color: red; text-align: center"><%=error %></p>
            <%} %>
        </div>
    </form>
</asp:Content>