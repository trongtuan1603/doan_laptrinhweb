﻿<%@ Page Title="Giỏ hàng" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="WebSaleLaptop.Cart" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/cart.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <%if (count > 0)
        {%>
        <form runat="server" style="margin-top: 130px;">
            <div class="order-line" style="margin-bottom: 5px">
                <ion-icon size="large" name="cart-outline"></ion-icon>
                <h4 style="margin-left: 5px"><b>Giỏ hàng của bạn</b></h4>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="maSP" HeaderText="Mã" Visible="False">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="0px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Hình ảnh">
                            <ItemTemplate>
                                <asp:Image ImageUrl=<%# string.Format("~/Assets/images/products/{0}",Eval("anhSP"))%> CssClass="img-product-cart" runat="server"/>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="tenSP" HeaderText="Tên sản phẩm" >
                        <ItemStyle Width="300px" Wrap="True" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="Số lượng">
                            <ItemTemplate>
                                <div style="display:flex; flex-direction: row; justify-content: center; align-items: center">
                                    <asp:Button ID="ButtonDecrease" runat="server" Text="-" CausesValidation="false" CommandArgument='<%# Eval("maSP") %>' CommandName="decrease" CssClass="btnChangeQuantity"/>
                                    <asp:Label ID="txtSoLuong" runat="server" Text='<%#Eval("soluong") %>' CssClass="quantity_text"></asp:Label>
                                    <asp:Button ID="ButtonIncrease" runat="server" Text="+" CausesValidation="false"  CommandArgument='<%# Eval("maSP") %>' CommandName="increase" CssClass="btnChangeQuantity"/>
                                </div>
                            </ItemTemplate>
                             <ControlStyle Font-Bold="True" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="giaban" HeaderText="Giá" DataFormatString="{0:0,0}đ" >
                        <ControlStyle Font-Bold="True" ForeColor="#3333FF" />
                        <ItemStyle Width="100px" HorizontalAlign="Center" Font-Bold="True" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:Button ID="Button1" runat="server" Text="Xóa giỏ hàng" OnClick="Button1_Click" CssClass="btn-remove-cart"/>
                </div>
                <div class="col-md-4">
                    <div class="order-info">
                        <h4><b>Thông tin thanh toán</b></h4>
                        <div class="border"></div>
                        <div class="order-line">
                            <p style="flex: 1">Tạm tính</p>
                            <p><b><%=String.Format("{0:0,0}", total_amount)%>đ</b></p>
                        </div>
                        <div class="order-line">
                            <p style="flex: 1">Thanh toán</p>
                            <p><b>COD</b></p>
                        </div>
                        <div class="order-line">
                            <h5  style="flex: 1">Tổng tiền</h5>
                            <h4 style="color: red"><b><%=String.Format("{0:0,0}", total_amount)%>đ</b></h4>
                        </div>
                        <asp:Button ID="btnCheckout" runat="server" Text="Tiến hành đặt hàng" CssClass="btn-checkout" OnClick="btnCheckout_Click"/>
                    </div>
                    
                </div>
            </div>

        </form>     
    <%} else {%>
        <form runat="server">
            <div style="display: flex; align-items: center; margin-top: 200px; flex-direction: column">
                <img src="Assets/images/empty_cart.png" alt="empty" />
                <h4>Giỏ hàng của bạn đang trống !</h4>
                <a href="Default.aspx" class="btn-continue">Tiếp tục mua hàng</a>
            </div>
        </form>
    <%}%>
    <style>
        .btn-continue{
          background-color: #008CBA; 
          border: none;
          color: white;
          padding: 10px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin-top: 40px;
          border-radius: 20px;
        }
        .btn-continue:hover{
          background-color: #008CBA; 
          border: none;
          color: white;
          padding: 10px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin-top: 40px;
          border-radius: 20px;
        }
        table {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
          border-collapse: collapse;
          border-radius: 5px;
          overflow: hidden;
          border: none;

        }

        th, td{
          padding: 1em;
          background: #f5f5f5;
          border: none;
        }

        table tr:nth-child(even){background-color: #FFF;}

        table tr:hover {background-color: #FFF;}

        table th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #008CBA;
          color: white;
        }
    </style>
</asp:Content>
