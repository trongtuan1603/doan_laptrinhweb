﻿<%@ Page Title="Laptop140" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSaleLaptop._Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="Assets/css/product.css" rel="stylesheet" type="text/css" runat="server" media="screen"/>
    <form runat="server" style="background-color: white; min-height: 900px">
        <div class="container" style="padding-top: 120px; padding-left: 0; ">
        <div class="col-md-8" style="padding: 0; margin-right: 0px;">
            <%--<img style="max-height: 310px; width: 100%;" src="Assets/images/Banner-main-01.png" />--%>
            <div id="slider">
            <figure>
                <img src="Assets/images/Banner-gaming_laptops.jpg" alt="">
                <img src="Assets/images/images_brands_asus_2020_2020_asus_rog_banner.jpg" alt="">
                <img src="Assets/images/KV_Banner_large.jpg" alt="">
                <img src="Assets/images/Banner-main-01.png" alt="">
                <img src="Assets/images/Asus-Vivobook-S14-Colors.png" alt="">
            </figure>
            </div>

        </div>
        <div class="col-md-4" style="padding-left: 0; ">
            <div style="margin-left: 20px; border: 0.1px solid #eeeeee;">
                <div>
                    <a href="#" id="title">TIN TỨC CÔNG NGHỆ</a>
                </div>
                <%foreach (var item in news){ %>
                    <div class="row" style="margin-left: 8px; margin-bottom: 10px; margin-top: 10px;">
                        <div class="col-md-3" style="padding-left: 0">
                            <img src="Assets/images/news/<%=item.Image%>" class="img-news" />
                        </div>
                        <div class="col-md-9" >
                            <a href="DetailNew.aspx?newsid=<%=item.ID%>" class="title-text"><%=item.Title%></a>
                        </div>
                     </div> 
                <%}%>
         </div>
            </div>
    </div>

    <div class="container" style="padding: 0">
       <div class="row">
            <div class="container" style="padding: 0">
<%--                <div id="border-brands">
                    <p id="title-brand">THƯƠNG HIỆU</p>
                </div>--%>
                <div id="list-brand">
                    <%foreach (var item in brands) { %>
                        <div class="item-brand" onclick="viewByBrand('<%=item.HangSX%>')" style="cursor:pointer;">
                            <img src="Assets/images/brands/<%=item.Logo%>" alt="Alternate Text" />
                        </div>
                    <%}%>
                </div>
            </div>
            <div class="container" style="padding: 0; display: flex; flex-direction: row; margin-bottom: 20px; align-items: center">
                <button onserverclick="Ascending_Price" runat="server" class="btn-filter">Giá tăng dần</button>
                <button onserverclick="Descending_Price" runat="server" class="btn-filter">Giá Giảm dần</button>
                <button onserverclick="SortByName" runat="server" class="btn-filter">Tên từ A đến Z</button>
                <button onserverclick="SortByNameDescending" runat="server" class="btn-filter">Tên từ Z đến A</button>
                <button onserverclick="SortByNameDescending" runat="server" class="btn-filter">Mới nhất</button>
                <button onserverclick="SortByNameDescending" runat="server" class="btn-filter">Bán chạy</button>
<%--                <span style="margin: 10px 14px 0 0">Giá từ</span>
                <input class="range-input" type="text" name="price" value="" />
                <span style="margin:  10px 14px 0 0">đến</span>
                <input class="range-input" type="text" name="price" value="" />
                <asp:Button ID="Button1" runat="server" Text="Lọc" CssClass="range-filter-btn"/>--%>
            </div>
        </div>
    </div>
    <%if (searchStr != null) { %>
        <h4>Kết quả cho từ khóa tìm kiếm: "<%=searchStr%>"</h4>
    <%}%>
    <%if (brandID != null) { %>
        <h4>Sản phẩm của thương hiệu <%=brandName%></h4>
    <%}%>
    <div class="container" runat="server" style="padding: 0">
        <div class="col-md-2" style="margin-top: 5px; background-color: #f1f1f1; padding-top: 15px; border-radius: 5px">
            <span style="font-weight: 600; margin-left: -5px; ">Mức giá</span>
            <asp:RadioButtonList ID="FilterPrice" runat="server" OnSelectedIndexChanged="CheckBoxPriceChange" AutoPostBack="true" CssClass="filter-product">
                <asp:ListItem Value="0,10">Dưới 10 triệu</asp:ListItem>
                <asp:ListItem Value="10,15">Từ 10 - 15 triệu</asp:ListItem>
                <asp:ListItem Value="15,20">Từ 15 - 20 triệu</asp:ListItem>
                <asp:ListItem Value="20,1000">Trên 20 triệu</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <span style="font-weight: 600; margin-left: -5px; ">Theo nhu cầu</span>
            <asp:RadioButtonList ID="FilterNeed" runat="server" OnSelectedIndexChanged="CheckBoxPriceChange" AutoPostBack="True" CssClass="filter-product" DataSourceID="SqlDataSource1" DataTextField="tenloai" DataValueField="maloai">
            </asp:RadioButtonList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QLWebBanLaptopConnectionString %>" SelectCommand="SELECT [MaLoai], [TenLoai] FROM [LoaiSP]"></asp:SqlDataSource>
            
            </div>
        <div class="col-md-10" style="padding: 0 0 0 12px;">
           <%if (products.Count > 0){ %>
                <%for(int i=0; i < offset; i++){ %>
                    <div class="item-product" onclick="viewDetail('DetailProduct.aspx?id=<%=products[i].MaSP%>')">
                        <div>
                            <div class="img-product">
                                <img class="img" src="Assets/images/products/<%=products[i].AnhSP%>" alt="img-product"/>
                            </div>
                            <p class="price-text"><%=String.Format("{0:0,0}", products[i].GiaBan) %>₫</p>
                            <p class="name"#"><%=products[i].TenSP%></p>
                            <div class="thongso">
                                <img src="Assets/images/cpu.jpg" class="icon-thongso" />
                                <p><%=products[i].CPU%></p>
                            </div>
                            <div class="thongso">
                                <div style="flex: 1; flex-direction: row; display: flex; align-items: center">
                                    <img src="Assets/images/ram.png" class="icon-thongso" />
                                    <p><%=products[i].RAM%></p>
                                </div>
                                <div class="thongso">
                                    <img src="Assets/images/storage.jpg" class="icon-thongso" />
                                    <p><%=products[i].BoNho%></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <%}%>
            
           <%} %>
            <div style="display: flex; justify-content: center" runat="server" id="loadmore">
                <asp:Button ID="Button1" runat="server" Text="Xem thêm" OnClick="Button1_Click" CssClass="btn-loadmore"/>
            </div>
            <asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" BorderStyle="None" />
        </div>
    </div>
    </form>
    <script>
        function viewDetail(link) {
            window.location.href = link;
        }

        function viewByBrand(link) {
            window.location.href = "Default.aspx?brandid=" + link;
        }

        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }

    </script>
    <style>
        @keyframes slidy {
        0% { left: 0%; }
        20% { left: 0%; }
        25% { left: -100%; }
        45% { left: -100%; }
        50% { left: -200%; }
        70% { left: -200%; }
        75% { left: -300%; }
        95% { left: -300%; }
        100% { left: -400%; }
        }

        body { margin: 0; } 
        div#slider { overflow: hidden; }
        div#slider figure img { width: 20%; float: left; max-height: 310px }
        div#slider figure {
            position: relative;
            width: 500%;
            margin: 0;
            left: 0;
            text-align: left;
            font-size: 0;
            animation: 30s slidy infinite;
        }
        .btn-loadmore {
            background-color: #FFF; 
            border: #0094ff 1px solid;
            color: #0094ff;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin-top: 20px;
            border-radius: 5px;
        }

    </style>
</asp:Content>
