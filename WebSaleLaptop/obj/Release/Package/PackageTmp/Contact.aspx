﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebSaleLaptop.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/form.css" rel="stylesheet" />
    <h2><%: Title %>.</h2>
    <div class="container">
        <div class="row"  style="margin-top: 125px; height: 55vh" >
            <div class="col-lg-6" style="padding: 10px">
                <h3>Liên hệ với chúng tôi</h3>
                <h5 style="line-height: 20pt">Chào mừng bạn đến với bộ phận Hỗ trợ khách hàng của Laptop140, thời gian làm việc của chúng tôi là từ 09:00 - 18:00 từ thứ 2 đến Chủ Nhật. Vui lòng điền vào mẫu dưới đây trước khi bắt đầu cuộc trò chuyện.<br /> Laptop140 coi trọng và tôn trọng quyền riêng tư của bạn và sẽ không chia sẻ nội dung này với bên thứ ba.</h5>
            </div>
            <div class="col-lg-6">
                <form class="form-column" runat="server">
                    <h3>Điền thông tin của bạn</h3>
                    <asp:TextBox ID="txtName" runat="server" placeholder="Họ tên của bạn"></asp:TextBox>
                    <asp:TextBox ID="txtSDT" runat="server" placeholder="Số điện thoại của bạn"></asp:TextBox>
                    <asp:TextBox ID="TxtTenHang" runat="server" placeholder="Email của bạn"></asp:TextBox>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h3>Thắc mắc về sản phẩm/ dịch vụ</h3>
                <p>Nếu Quý khách có bất kỳ thắc mắc nào liên quan đến các sản phẩm, dịch vụ của chúng tôi, vui lòng tham khảo mục Câu Hỏi Thường GặpCâu Hỏi Thường Gặp để tìm hiểu thêm về các câu hỏi thường gặp liên quan đến sản phẩm và dịch vụ của chúng tôi để tìm nhanh câu trả lời. Ngoài ra Quý khách cũng có thể lựa chọn cách thức liên lạc phù hợp để chúng tôi có thể hỗ trợ Quý khách tốt nhất.</p>
            </div>
            <div class="col-lg-4">
                <h3>Khen ngợi nhân viên chúng tôi</h3>
                Tại Laptop140, chúng tôi cam kết mang đến cho Quý khách trải nghiệm đẳng cấp thế giới với các sản phẩm và dịch vụ tối ưu.
                <br />
                Nếu Quý khách cảm thấy hài lòng khi làm việc với chúng tôi hoặc đơn giản đã có những trải nghiệm tuyệt vời, chúng tôi rất mong được lắng nghe những khen ngợi của Quý khách.
            </div>
            <div class="col-lg-4">
                <h3>Đóng góp ý kiến</h3>
                <p>Ý kiến đóng góp của Quý khách rất quý giá, giúp chúng tôi phục vụ Quý khách và tất cả khách hàng ngày càng tốt hơn.
                    <br />
                Những ý kiến này cũng giúp chúng tôi duy trì tiêu chuẩn cao với các sản phẩm và dịch vụ và đáp ứng nhu cầu khách hàng tối ưu nhất.
            </div>
    </div>
</asp:Content>
