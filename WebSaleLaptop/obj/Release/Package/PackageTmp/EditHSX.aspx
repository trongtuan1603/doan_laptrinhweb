﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditHSX.aspx.cs" Inherits="WebSaleLaptop.EditHSX" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <link href="Assets/css/form.css" rel="stylesheet" />
        <link href="Assets/css/admin.css" rel="stylesheet" />

    <div class="p-title-page"> 
       <ion-icon name="people-outline"></ion-icon>
       <p class="title-page"> Sửa thương hiệu</p>
    </div>
    <div class="form-column" style="width: 600px;">
        <div class="line">
        <asp:Label ID="LbTenHang" runat="server" Text="Tên hãng"></asp:Label>
             <asp:RequiredFieldValidator ID="ReqTenHang" ErrorMessage="Vui lòng nhập tên hàng" 
        ControlToValidate="TxtTenHang" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtTenHang" runat="server"></asp:TextBox>
    <div class="line">
        <asp:Label Text="Logo" runat="server" />
            <asp:RequiredFieldValidator ID="RequpladImg" ErrorMessage="Vui lòng chọn logo" 
        ControlToValidate="TxtLogo" runat="server" ForeColor="Red" />
    </div>
        <div class="line">
                 <asp:FileUpload ID="uploadImg" runat="server" onchange="ChonAnh(this.value)"/>
                <asp:TextBox ID="TxtLogo" runat="server"></asp:TextBox>    
        </div>

     <asp:Button ID="BtnUpdate" runat="server" Text="Cập nhật"  /> 
    <asp:Label ID="Lberror" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
    <script>
            function ChonAnh(a) {
                document.getElementById('<%=TxtLogo.ClientID %>').value = a.split("\\").pop();
        }
    </script>
</asp:Content>
