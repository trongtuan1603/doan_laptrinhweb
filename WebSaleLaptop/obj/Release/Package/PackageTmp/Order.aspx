﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="WebSaleLaptop.Order" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/cart.css" rel="stylesheet" />
    <form runat="server">
        <div class="container" style="margin-top: 135px; display: flex; flex-direction: row; justify-content: center">
            <%if (!success) { %>
                <div class="order-info-checkout">
                <h4><b>Thông tin đơn hàng</b></h4>
                <div class="border"></div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Tên khách hàng</p>
                    <p><b><%=kh.TenKH%></b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Số điện thoại liên hệ</p>
                    <p><b><%=kh.SDT%></b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Địa chỉ email</p>
                    <p><b><%=kh.Email%></b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Địa chỉ giao hàng</p>
                    <p><b><%=kh.DiaChi%></b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Thời gian đặt hàng</p>
                    <p><b><%=DateTime.Now.ToString()%></b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Tạm tính</p>
                    <p><b><%=String.Format("{0:0,0}", total_amount)%>đ</b></p>
                </div>
                <div class="order-line-checkout">
                    <p style="flex: 1">Thanh toán</p>
                    <p><b>COD</b></p>
                </div>
                <div class="order-line-checkout">
                    <h5  style="flex: 1">Tổng tiền</h5>
                    <h4 style="color: red"><b><%=String.Format("{0:0,0}", total_amount)%>đ</b></h4>
                </div>
                <div class="footer-order">
                    <asp:Button ID="btnCheckout" runat="server" Text="Đặt hàng" CssClass="btn-order" OnClick="btnCheckout_Click"/>
                </div>
            </div>
            <%} else {%>
                <h4 class="success-panel">Đặt hàng thành công. Chúng tôi sẽ liên hệ với quý khách trong thời gian sớm nhất. Xin cảm ơn</h4>
            <%}%>
        </div>
    </form>
    <script>
        function goHome() {
            window.location.href = 'Default.aspx';
        }
    </script>
    <style>
        .btn-order {
            width: 200px;
            background-color: #0092ff;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin-top: 20px;
            border-radius: 5px;
            font-weight: bold;
        }
        .order-line-checkout {
            display: flex;
            flex-direction: row;
            align-items: center;
            padding: 10px 0;
        }
        .footer-order {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
        }
        .order-info-checkout {
            display: flex;
            flex-direction: column;
            padding: 10px;
            background-color: #f5f5f5;
            border-radius: 5px;
            width: 800px;   
        }
        .success-panel {
            padding: 20px;
            background-color: #d8ffc6;
            color: #0a9000;
            width: 800px;
            border-radius: 10px;
            margin-top: 50px;
        }
    </style>
</asp:Content>
