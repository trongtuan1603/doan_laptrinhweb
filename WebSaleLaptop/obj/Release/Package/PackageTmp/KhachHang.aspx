﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="KhachHang.aspx.cs" Inherits="WebSaleLaptop.KhachHang1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page"> Quản lý khách hàng</p>
    </div>
    <div>
        <div class="add-and-find">
            <a href="AddCustomer.aspx">Thêm khách hàng</a>  
            <h5> Tìm kiếm khách hàng </h5>
            <asp:TextBox ID="TxtTimkiem" runat="server"></asp:TextBox>
            <asp:Button ID="BntTim" runat="server" Text="Tìm" OnClick="BntTim_Click" /><br />
        </div>
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" AutoGenerateColumns="False" CssClass="table-manager">
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                 <Columns>
                     <asp:BoundField DataField="MaKH" HeaderText="Mã khách hàng" />
                     <asp:BoundField DataField="TenKH" HeaderText="Tên khách hàng" />
                     <asp:BoundField DataField="DiaChi" HeaderText="Địa Chỉ" />
                     <asp:BoundField DataField="SDT" HeaderText="SĐT" />
                     <asp:BoundField DataField="NgaySinh" HeaderText="Ngày sinh" />
                     <asp:BoundField DataField="GioiTinh" HeaderText="Giới tính" />
                     <asp:BoundField DataField="TaiKhoan" HeaderText="Tài khoản" />
                     <asp:BoundField DataField="MatKhau" HeaderText="Mật khẩu" />
                     <asp:BoundField DataField="Email" HeaderText="Email" />
                    <asp:CommandField ShowDeleteButton="True"/>
                </Columns>
                <Columns>
                     <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandArgument='<%# Eval("MaKH") %>' Text="Chỉnh sửa" onclick="Details_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
        </asp:GridView>
            <br />
    </div>
    <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
        
    </style>
</asp:Content>
