﻿<%@ Page Title="Tin tức" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetailNew.aspx.cs" Inherits="WebSaleLaptop.DetailNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form style="margin-top: 110px;">
        <img class="img-detail-news" src="Assets/images/news/<%=news.Image%>" alt="img-new" />
        <h3 class="title-detail"><%=news.Title%></h3>
        <p class="news-content"><%=news.Content%></p>
    </form>
    <style>
        .title-detail {
            margin: 20px 0 15px 0;
            padding: 0;
        }
        .img-detail-news {
            width: 100%;
            flex: 0.5
        }
        .news-content {
            font-size: 13pt
        }
    </style>
</asp:Content>
