﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditSP.aspx.cs" Inherits="WebSaleLaptop.EditSP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="Assets/css/form.css" rel="stylesheet" />
        <link href="Assets/css/admin.css" rel="stylesheet" />

    <div class="p-title-page"> 
       <ion-icon name="people-outline"></ion-icon>
       <p class="title-page"> Thêm sản phẩm</p>
    </div>
    <div class="form-column" style="width: 500px;">
        <div class="line">
        <asp:Label ID="LbSP" runat="server" Text="Tên Sản Phẩm"></asp:Label>
     <asp:RequiredFieldValidator ID="ReqTenSP" ErrorMessage="Vui lòng nhập tên sản phẩm" 
        ControlToValidate="TxtTenSp" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtTenSp" runat="server"></asp:TextBox>
    <div class="line">
        <asp:Label ID="LbGB" runat="server" Text="Gía Bán"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqGiaBan" ErrorMessage="Vui lòng nhập giá bán" 
        ControlToValidate="TxtGiaBan" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtGiaBan" runat="server"></asp:TextBox>
    <div class="line">
        <asp:Label ID="LbRam" runat="server" Text="Ram"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqRam" ErrorMessage="Vui lòng nhập ram" 
        ControlToValidate="TxtRam" runat="server" ForeColor="Red" />
    </div> 
    <asp:TextBox ID="TxtRam" runat="server"></asp:TextBox>
    <div class="line">
     <asp:Label ID="LbCPU" runat="server" Text="CPU"></asp:Label>
     <asp:RequiredFieldValidator ID="ReqCPU" ErrorMessage="Vui lòng nhập CPU" 
        ControlToValidate="TxtCPU" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtCPU" runat="server"></asp:TextBox>
    <div class="line">
        <asp:Label ID="LbBoNho" runat="server" Text="Bộ nhớ"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqBN" ErrorMessage="Vui lòng nhập bộ nhớ" 
        ControlToValidate="TxtBN" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtBN" runat="server"></asp:TextBox>
    <div class="line">
         <asp:Label ID="LbMoTa" runat="server" Text="Mô tả"></asp:Label>
         <asp:RequiredFieldValidator ID="ReqMT" ErrorMessage="Vui lòng nhập mô tả" 
        ControlToValidate="TxtMT" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtMT" runat="server"></asp:TextBox>
    <div>
        <asp:Label ID="LbNgayNhap" runat="server" Text="Ngày nhập"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqNN" ErrorMessage="Vui lòng nhập ngày" 
        ControlToValidate="TxtNN" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtNN" runat="server"></asp:TextBox>

    <div class="line">
        <asp:Label Text="Ảnh sản phẩm" runat="server" />
        <asp:RequiredFieldValidator ID="RequpladImg" ErrorMessage="Vui lòng chọn ảnh" ControlToValidate="uploadImg" runat="server" ForeColor="Red" />
    </div>
    <asp:FileUpload ID="uploadImg" runat="server" />
    <div class="line">
        <asp:Label ID="lbSL" runat="server" Text="Số lượng"></asp:Label>
         <asp:RequiredFieldValidator ID="ReqSL" ErrorMessage="Vui lòng nhập số lượng" 
        ControlToValidate="TxtML" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtSL" runat="server"></asp:TextBox>
    <div class="line"> 
        <asp:Label ID="LbML" runat="server" Text="Mã loại sản phẩm"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqML" ErrorMessage="Vui lòng nhập mã loại" 
        ControlToValidate="TxtML" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtML" runat="server"></asp:TextBox>
    <div class="line"> 
         <asp:Label ID="LbHangSX" runat="server" Text="Hãng sản xuất"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqHSX" ErrorMessage="Vui lòng nhập hãng sản xuất " 
        ControlToValidate="TxtHSX" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtHSX" runat="server"></asp:TextBox>
    <div class="line">
        <asp:Label ID="LbSPMoi" runat="server" Text="Sản phẩm mới"></asp:Label>
        <asp:RequiredFieldValidator ID="ReqSPMoi" ErrorMessage="Vui lòng nhập sản phẩm mới" 
        ControlToValidate="TxtSPMoi" runat="server" ForeColor="Red" />
    </div>
    <asp:TextBox ID="TxtSPMoi" runat="server"></asp:TextBox>
    <asp:Button ID="BtnUpdate" runat="server" Text="Update"  /> 
    <asp:Label ID="Lberror" runat="server" Text="" ForeColor="Red"></asp:Label>
</asp:Content>
