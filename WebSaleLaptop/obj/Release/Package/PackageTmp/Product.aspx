﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="WebSaleLaptop.Product" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Assets/css/admin.css" rel="stylesheet" />
    <div class="p-title-page"> 
        <ion-icon name="people-outline"></ion-icon>
        <p class="title-page"> Quản lý sản phẩm</p>
    </div>
    <div>
        <div class="add-and-find">
            <a href="AddSP.aspx"> Thêm sản phẩm</a>
            <h5>Tìm kiếm sản phẩm</h5>
            <asp:TextBox ID="TxtTimkiem" runat="server"></asp:TextBox>
            <asp:Button ID="BntTim" runat="server" Text="Tìm" OnClick="BntTim_Click" />
        </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Height="215px" OnRowDeleting="GridView1_RowDeleting" CssClass="table-manager" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="MaSP" HeaderText="Mã sản phẩm" />
            <asp:BoundField DataField="TenSP" HeaderText="Tên sản phẩm" />
            <asp:BoundField DataField="GiaBan" HeaderText="Gía Bán" />
            <asp:BoundField DataField="Ram" HeaderText="Ram" />
            <asp:BoundField DataField="CPU" HeaderText="CPU" />
            <asp:BoundField DataField="BoNho" HeaderText="Bộ nhớ" />
            <asp:BoundField DataField="MoTa" HeaderText="Mô tả" />
            <asp:BoundField DataField="NgayNhap" HeaderText="Ngày nhập" />
            <asp:BoundField DataField="AnhSP" HeaderText="Ảnh sản phẩm" />
            <asp:BoundField DataField="SoLuong" HeaderText="Số lượng" />
            <asp:BoundField DataField="MaLoaiSP" HeaderText="Mã loại sản phẩm" />
            <asp:BoundField DataField="HangSX" HeaderText="Hãng sản xuất" />
            <asp:BoundField DataField="SPMoi" HeaderText="Sản phẩm mới" />
            <asp:CommandField ShowDeleteButton="True" />
<asp:TemplateField><ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandArgument='<%# Eval("MaSP") %>' Text="Edit" onclick="Details_Click"></asp:LinkButton>
                        
</ItemTemplate>
</asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
        </div>
    <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
    </style>
</asp:Content>
