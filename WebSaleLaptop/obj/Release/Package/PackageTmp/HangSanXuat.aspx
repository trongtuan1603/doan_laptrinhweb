﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HangSanXuat.aspx.cs" Inherits="WebSaleLaptop.HangSanXuat1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <link href="Assets/css/form.css" rel="stylesheet" />
        <link href="Assets/css/admin.css" rel="stylesheet" />

    <div class="p-title-page"> 
       <ion-icon name="people-outline"></ion-icon>
       <p class="title-page">Quản lý thương hiệu</p>
    </div>
    <div class="add-and-find">
        <a href="AddHSX.aspx"> Thêm thương hiệu</a>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Height="185px" OnRowDeleting="GridView1_RowDeleting" CssClass="table-manager" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="HangSX" HeaderText="Hãng sản xuất" />
            <asp:BoundField DataField="TenHang" HeaderText="Tên Hàng" />
            <asp:BoundField DataField="Logo" HeaderText="Logo" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <Columns>
               <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="EditButton" runat="server" CommandArgument='<%# Eval("HangSX") %>' Text="Edit" onclick="Details_Click"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
        <style>
        .table-manager {
            width: 100%
        }
        .table-manager th {
            padding: 10px;
        }
        .table-manager td {
            padding: 10px;
        }
        
    </style>
</asp:Content>
