﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditCustomer.aspx.cs" Inherits="WebSaleLaptop.EditCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Assets/css/form.css" rel="stylesheet" />
    <link href="Assets/css/admin.css" rel="stylesheet" />

    <div class="p-title-page"> 
       <ion-icon name="people-outline"></ion-icon>
       <p class="title-page"> Thêm khách hàng</p>
    </div>
    <div class="form-column">
        <div class="line">
            <asp:Label ID="Label2" runat="server" Text="Tên khách hàng" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="ReqTenKH" ErrorMessage="Vui lòng nhập tên khách hàng" 
            ControlToValidate="txtName" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label1" runat="server" Text="Địa chỉ" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Vui lòng địa chỉ" 
            ControlToValidate="txtAddress" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label6" runat="server" Text="Số điện thoại" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Vui lòng nhập số điện thoại" 
            ControlToValidate="txtPhone" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label3" runat="server" Text="Ngày sinh" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Vui lòng nhập ngày sinh" 
            ControlToValidate="txtBirth" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <input type="date" name="name" id="txtBirth" value="" runat="server"/>
        <div class="line">
            <asp:Label ID="Label4" runat="server" Text="Giới tính" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Vui lòng nhập giới tính" 
            ControlToValidate="txtGender" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="txtGender" runat="server"></asp:TextBox>
        <div class="line">
            <asp:Label ID="Label5" runat="server" Text="Giới tính" CssClass="label-form"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Vui lòng nhập email" 
            ControlToValidate="txtEmail" runat="server" ForeColor="Red"  CssClass="val"/>
        </div>
        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Lưu" OnClick="Button1_Click"/>
    </div>
    <style>
        input[type=date] {
            width: 100%;
            padding: 6px 10px;
            margin: 10px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
    </style>
</asp:Content>
