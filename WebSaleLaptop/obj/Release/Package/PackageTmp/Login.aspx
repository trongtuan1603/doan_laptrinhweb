﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebSaleLaptop.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Assets/css/resgister.css" rel="stylesheet" />
    <form runat="server" class="register-form">
        <div class="form-column" runat="server" style="width: 400px; box-shadow: 0 0 5px gray; border-radius: 5px; padding: 20px">
                <h4 style="text-align: center; margin-top: 20px">Đăng nhập</h4>
                <asp:Label ID="lblPhone" runat="server" Text="Số điện thoại hoặc email"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Vui lòng nhập số điện thoại" ControlToValidate="txtPhone" ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPhone" runat="server" CssClass="info-feild" OnTextChanged="txtPhone_TextChanged"></asp:TextBox>
                <asp:Label ID="lblPassword" runat="server" Text="Mật khẩu"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Mật khẩu không được để trống" ControlToValidate="txtPassword"  ForeColor="Red" CssClass="validator-register"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="info-feild"></asp:TextBox>
                <asp:Button ID="btnLogin" runat="server" Text="Đăng nhập" CssClass="btn-register" OnClick="btnLogin_Click"/>
                <p style="color: red; text-align: center"><%=error %></p>
        </div>
    </form>
</asp:Content>
