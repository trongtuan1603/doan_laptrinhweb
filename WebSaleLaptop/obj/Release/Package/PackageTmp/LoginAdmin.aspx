﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginAdmin.aspx.cs" Inherits="WebSaleLaptop.LoginAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="login-admin">
                <p class="title">Đăng nhập để tiếp tục</p>
                <div class="panel">
                    <div class="line">
                    <ion-icon name="person-outline"></ion-icon>
                    <p>Tên đăng nhập</p>
                    </div>
                    <input type="text" name="name" id="username" value="" runat="server" class="text-box"/>
                    <div class="line">
                    <ion-icon name="lock-closed-outline"></ion-icon>
                    <p>Mật khẩu</p>
                    </div>
                    <input type="password" name="name" id="password" value="" runat="server" class="text-box"/>
                    <asp:Button ID="btnLogin" runat="server" Text="Đăng nhập" OnClick="btnLogin_Click" />
                    <asp:Label ID="lblError" runat="server" Font-Size="11pt" ForeColor="Red" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>

<style>
    *{
        margin: 0;
        padding: 0;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
    }
    .main {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        background-image: url('Assets/images/backgound_login.jpg');

    }
    .login-admin {
        background-color: #fff;
        box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
        width: 370px;
        display: flex;
        flex-direction: column;
        border-radius: 10px;
        overflow: hidden

    }
    .title {
        padding: 20px;
        font-weight: 500;
        text-align: center;  
        background-color: #00a9ff;
        color: #fff
    }
    .panel {
        padding: 5px 30px 30px 30px ;
    }
    .text-box {
        width: 310px;
        padding: 12px 10px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
    input[type=submit] {
        width: 310px;
        background-color: #00a9ff;
        color: white;
        padding: 10px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-weight: 500;
    }
    .text-box:focus {
        outline: none
    }
    .line {
        display: flex;
        flex-direction: row;
        align-items: center;
        margin-top: 10px;
    }
        .line ion-icon {
            margin-right: 10px;
        }
        .line input {
            
        }
</style>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
